package com.nespalier.generateurRepas.services.impl;

import com.nespalier.generateurRepas.model.Annee;
import com.nespalier.generateurRepas.repository.IAnneeRepo;
import com.nespalier.generateurRepas.services.IAnneeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Service
public class AnneeServiceImpl implements IAnneeService {
    @Autowired
    IAnneeRepo anneeRepo;

    @Override
    public Annee creerAnnee(Annee annee) {
        Annee anneeAcreer = new Annee();
        if(!ObjectUtils.isEmpty(annee)){
            anneeAcreer = anneeRepo.save(annee);
            return anneeAcreer;
        }
        return anneeAcreer;
    }

    @Override
    public Annee recupererAnneeById(Long idAnnee) {
        return null;
    }

    @Override
    public List<Annee> listeAnnee() {
        return null;
    }
}
