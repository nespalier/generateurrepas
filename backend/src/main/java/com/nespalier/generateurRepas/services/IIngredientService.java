package com.nespalier.generateurRepas.services;

import com.nespalier.generateurRepas.model.Ingredient;

import java.util.List;

public interface IIngredientService {

    public Ingredient ajouterIngredient(Ingredient ingredient);
    public Ingredient recupererIngredientParId(Long idIngredient);
    public void supprimerIngredientParId(Long idIngredient);
    public List<Ingredient> listeIngredients();
}
