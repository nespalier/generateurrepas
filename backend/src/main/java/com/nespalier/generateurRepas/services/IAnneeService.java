package com.nespalier.generateurRepas.services;

import com.nespalier.generateurRepas.model.Annee;

import java.util.List;

public interface IAnneeService {

    public Annee creerAnnee(Annee annee);
    public Annee recupererAnneeById(Long idAnnee);
    public List<Annee> listeAnnee();
}
