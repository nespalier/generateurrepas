package com.nespalier.generateurRepas.services.impl;

import com.nespalier.generateurRepas.model.Ingredient;
import com.nespalier.generateurRepas.repository.IIngredientRepo;
import com.nespalier.generateurRepas.services.IIngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Service
public class IngredientServiceImpl implements IIngredientService {
    @Autowired
    IIngredientRepo ingredientRepo;

    @Override
    public Ingredient ajouterIngredient(Ingredient ingredient) {
        Ingredient ingredientAcreer = new Ingredient();
        if (!ObjectUtils.isEmpty(ingredient)) {
            return ingredientRepo.save(ingredient);
        }
        return ingredientAcreer;
    }

    @Override
    public Ingredient recupererIngredientParId(Long idIngredient) {
        if(idIngredient != null){
            return ingredientRepo.findById(idIngredient).get();
        }
        return null;
    }

    @Override
    public void supprimerIngredientParId(Long idIngredient) {

    }

    @Override
    public List<Ingredient> listeIngredients() {
        return null;
    }
}
