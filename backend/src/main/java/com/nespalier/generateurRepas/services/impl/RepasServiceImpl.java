package com.nespalier.generateurRepas.services.impl;

import com.nespalier.generateurRepas.model.Annee;
import com.nespalier.generateurRepas.model.Ingredient;
import com.nespalier.generateurRepas.model.Repas;
import com.nespalier.generateurRepas.repository.IAnneeRepo;
import com.nespalier.generateurRepas.repository.IRepasRepo;
import com.nespalier.generateurRepas.services.IRepasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import java.util.List;

@Service
public class RepasServiceImpl implements IRepasService {
    @Autowired
    IRepasRepo repasRepo;

    @Override
    public Repas ajouterRepas(Repas repas) {
        Repas repasAcreer = new Repas();
        List<Ingredient> ingredients = repas.getIngredients();
        if (!ObjectUtils.isEmpty(repasAcreer) && !CollectionUtils.isEmpty(ingredients)) {
            return repasAcreer = repasRepo.save(repas);
        }
        return repasAcreer;
    }

    @Override
    public Repas recupererRepasParId(Long idRepas) {
        Repas repasArecuperer = new Repas();
        if (idRepas != null) {
            return repasArecuperer = repasRepo.findById(idRepas).get();
        }
        return repasArecuperer;
    }

    @Override
    public void supprimerRepasParId(Long idRepas) {

    }

    @Override
    public List<Repas> listeRepas() {
        return repasRepo.findAll();
    }
}
