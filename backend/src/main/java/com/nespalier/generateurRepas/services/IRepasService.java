package com.nespalier.generateurRepas.services;

import com.nespalier.generateurRepas.model.Ingredient;
import com.nespalier.generateurRepas.model.Repas;

import java.util.List;

public interface IRepasService {

    public Repas ajouterRepas(Repas repas);
    public Repas recupererRepasParId(Long idRepas);
    public void supprimerRepasParId(Long idRepas);
    public List<Repas> listeRepas();
}
