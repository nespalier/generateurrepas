package com.nespalier.generateurRepas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GenerateurRepasApplication {

	public static void main(String[] args) {
		SpringApplication.run(GenerateurRepasApplication.class, args);
	}

}
