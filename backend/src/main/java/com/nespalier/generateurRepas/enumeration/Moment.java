package com.nespalier.generateurRepas.enumeration;

public enum Moment {

    MATIN,
    MIDI,
    SOIR
}
