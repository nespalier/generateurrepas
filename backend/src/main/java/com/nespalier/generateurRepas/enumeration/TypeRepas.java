package com.nespalier.generateurRepas.enumeration;

public enum TypeRepas {
    ENTREE,
    PLAT,
    DESSERT,
    APERO

}
