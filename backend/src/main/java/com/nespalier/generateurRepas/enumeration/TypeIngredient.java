package com.nespalier.generateurRepas.enumeration;

public enum TypeIngredient {
    FRUITS,
    LEGUMES,
    VIANDE,
    CHARCUTERIE,
    PRODUITS_LAITIER,
    CONSERVES
}
