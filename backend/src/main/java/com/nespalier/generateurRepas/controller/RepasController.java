package com.nespalier.generateurRepas.controller;

import com.nespalier.generateurRepas.model.Repas;
import com.nespalier.generateurRepas.services.IRepasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/repas")
public class RepasController {

    @Autowired
    IRepasService repasService;

    @PostMapping
    public Repas ajouterRepas(@RequestBody Repas repas) {

        return repasService.ajouterRepas(repas);
    }

    @GetMapping(value = {"/{id}"})
    public Repas recupererRepasParId(@PathVariable("id") Long idRepas) {

        return repasService.recupererRepasParId(idRepas);
    }

    @GetMapping
    public List<Repas> recupererAllRepas() {

        return repasService.listeRepas();
    }


}
