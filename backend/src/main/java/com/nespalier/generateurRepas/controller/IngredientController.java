package com.nespalier.generateurRepas.controller;

import com.nespalier.generateurRepas.model.Ingredient;
import com.nespalier.generateurRepas.services.IIngredientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/ingredient")

public class IngredientController {
    @Autowired
    IIngredientService ingredientService;

    @PostMapping
    public Ingredient ajouterIngredient(@RequestBody Ingredient ingredient) {

        return ingredientService.ajouterIngredient(ingredient);
    }

    @GetMapping(value = {"/{id}"})
    public Ingredient recupererIngredient(@PathVariable("id") Long idIngredient) {

        return ingredientService.recupererIngredientParId(idIngredient);
    }
}
