package com.nespalier.generateurRepas.controller;

import com.nespalier.generateurRepas.model.Annee;
import com.nespalier.generateurRepas.services.IAnneeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
@RequestMapping("/annee")
public class AnneeController {

    @Autowired
    IAnneeService anneeService;
    
    @PostMapping
    public Annee ajouterAnnee(@RequestBody Annee annee) {

        return anneeService.creerAnnee(annee);
    }
    
}
