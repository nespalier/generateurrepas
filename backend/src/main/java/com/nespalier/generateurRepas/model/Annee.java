package com.nespalier.generateurRepas.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Annee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long anneeId;
    @Column(name = "annee")
    private int annee;

    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "menu_annee",
            joinColumns = { @JoinColumn(name = "menu_id") })
    private List<Menu> menuList;


    public Annee() {
    }

    public Annee(Long anneeId, int annee, List<Menu> menuList) {
        this.anneeId = anneeId;
        this.annee = annee;
        this.menuList = menuList;
    }

    public Annee(int annee, List<Menu> menuList) {
        this.annee = annee;
        this.menuList = menuList;
    }

    public Long getAnneeId() {
        return anneeId;
    }

    public void setAnneeId(Long anneeId) {
        this.anneeId = anneeId;
    }

    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    public List<Menu> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<Menu> menuList) {
        this.menuList = menuList;
    }

    @Override
    public String toString() {
        return "Annee{" +
                "anneeId=" + anneeId +
                ", annee=" + annee +
                ", menuList=" + menuList +
                '}';
    }
}
