package com.nespalier.generateurRepas.model;

import com.nespalier.generateurRepas.enumeration.TypeIngredient;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Ingredient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ingredientId;
    private String nomIngredient;
    private TypeIngredient typeIngredient;

    public Ingredient() {
    }

    public Ingredient(Long ingredientId, String nomIngredient, TypeIngredient typeIngredient) {
        this.ingredientId = ingredientId;
        this.nomIngredient = nomIngredient;
        this.typeIngredient = typeIngredient;
    }

    public Ingredient(String nomIngredient, TypeIngredient typeIngredient) {
        this.nomIngredient = nomIngredient;
        this.typeIngredient = typeIngredient;
    }

    public Long getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(Long ingredientId) {
        this.ingredientId = ingredientId;
    }

    public String getNomIngredient() {
        return nomIngredient;
    }

    public void setNomIngredient(String nomIngredient) {
        this.nomIngredient = nomIngredient;
    }

    public TypeIngredient getTypeIngredient() {
        return typeIngredient;
    }

    public void setTypeIngredient(TypeIngredient typeIngredient) {
        this.typeIngredient = typeIngredient;
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "ingredientId=" + ingredientId +
                ", nomIngredient='" + nomIngredient + '\'' +
                ", typeIngredient=" + typeIngredient +
                '}';
    }
}
