package com.nespalier.generateurRepas.model;

import com.nespalier.generateurRepas.enumeration.TypeRepas;

import javax.persistence.*;
import java.util.List;

@Entity
public class Repas {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long repasId;
    private String libelle;
    private TypeRepas typeRepas;
    @OneToMany(fetch = FetchType.LAZY)
    private List<Ingredient> ingredients;


    public Repas() {
    }

    public Repas(Long repasId, String libelle, TypeRepas typeRepas, List<Ingredient> ingredients) {
        this.repasId = repasId;
        this.libelle = libelle;
        this.typeRepas = typeRepas;
        this.ingredients = ingredients;
    }

    public Repas(String libelle, TypeRepas typeRepas, List<Ingredient> ingredients) {
        this.libelle = libelle;
        this.typeRepas = typeRepas;
        this.ingredients = ingredients;
    }

    public Long getRepasId() {
        return repasId;
    }

    public void setRepasId(Long repasId) {
        this.repasId = repasId;
    }

    public TypeRepas getTypeRepas() {
        return typeRepas;
    }

    public void setTypeRepas(TypeRepas typeRepas) {
        this.typeRepas = typeRepas;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public String toString() {
        return "Repas{" +
                "repasId=" + repasId +
                ", libelle='" + libelle + '\'' +
                ", typeRepas=" + typeRepas +
                ", ingredients=" + ingredients +
                '}';
    }
}
