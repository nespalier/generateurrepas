package com.nespalier.generateurRepas.model;

import com.nespalier.generateurRepas.enumeration.Jour;
import com.nespalier.generateurRepas.enumeration.Moment;
import com.nespalier.generateurRepas.enumeration.Semaine;

import javax.persistence.*;
import java.util.List;

@Entity
public class Menu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long menuId;
    private Semaine semaine;
    private Jour jour;
    private Moment moment;
    @OneToMany(fetch = FetchType.LAZY)
    private List<Repas> repasList;

    public Menu() {
    }

    public Menu(Semaine semaine, Jour jour, Moment moment, List<Repas> repasList) {
        this.semaine = semaine;
        this.jour = jour;
        this.moment = moment;
        this.repasList = repasList;
    }

    public Menu(Long menuId, Semaine semaine, Jour jour, Moment moment, List<Repas> repasList) {
        this.menuId = menuId;
        this.semaine = semaine;
        this.jour = jour;
        this.moment = moment;
        this.repasList = repasList;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public Semaine getSemaine() {
        return semaine;
    }

    public void setSemaine(Semaine semaine) {
        this.semaine = semaine;
    }

    public Jour getJour() {
        return jour;
    }

    public void setJour(Jour jour) {
        this.jour = jour;
    }

    public List<Repas> getRepasList() {
        return repasList;
    }

    public void setRepasList(List<Repas> repasList) {
        this.repasList = repasList;
    }

    public Moment getMoment() {
        return moment;
    }

    public void setMoment(Moment moment) {
        this.moment = moment;
    }

    @Override
    public String toString() {
        return "Menu{" +
                "menuId=" + menuId +
                ", semaine=" + semaine +
                ", jour=" + jour +
                ", moment=" + moment +
                ", repasList=" + repasList +
                '}';
    }
}
