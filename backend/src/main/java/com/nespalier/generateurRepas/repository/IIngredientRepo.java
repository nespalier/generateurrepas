package com.nespalier.generateurRepas.repository;

import com.nespalier.generateurRepas.model.Ingredient;
import com.nespalier.generateurRepas.model.Repas;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IIngredientRepo extends JpaRepository<Ingredient, Long> {
}
