package com.nespalier.generateurRepas.repository;

import com.nespalier.generateurRepas.model.Annee;
import com.nespalier.generateurRepas.model.Repas;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IRepasRepo extends JpaRepository<Repas, Long> {
}
