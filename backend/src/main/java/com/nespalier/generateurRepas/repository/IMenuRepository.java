package com.nespalier.generateurRepas.repository;

import com.nespalier.generateurRepas.model.Menu;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IMenuRepository extends JpaRepository<Menu, Long> {
}
