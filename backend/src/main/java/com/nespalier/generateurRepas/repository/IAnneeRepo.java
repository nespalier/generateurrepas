package com.nespalier.generateurRepas.repository;

import com.nespalier.generateurRepas.model.Annee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface IAnneeRepo extends JpaRepository<Annee, Long> {

    @Query(value="SELECT * FROM generateurrepas.annee c WHERE c.annee = ?1", nativeQuery = true)
    public Annee trouverAnneeParLibelle(int annee);
}
