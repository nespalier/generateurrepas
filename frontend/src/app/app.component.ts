import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'frontend';
  showModal:boolean;
  showIngredient=false;
  showRepas=true;
  ngOnInit(): void {}
  modalClosed(isClosed) {
    this.showModal = false;
  }

  showIngredientFonction(){
    this.showIngredient = true;
    this.showRepas = false;
  }
}
