import { IngredientsConsultationComponent } from './Ingredients/ingredients-consultation/ingredients-consultation.component';
import { IngredientsAjoutModifComponent } from './Ingredients/ingredients-consultation/ingredients-ajout-modif/ingredients-ajout-modif.component';
import { RepasConsultationComponent } from './repas/repas-consultation/repas-consultation.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AccueilComponent } from './accueil/accueil.component';

@NgModule({
  declarations: [
    AppComponent,
    RepasConsultationComponent,
    AccueilComponent,
    IngredientsConsultationComponent,
    IngredientsAjoutModifComponent
   ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {



}
