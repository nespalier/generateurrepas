import { IngredientVo } from './../../classes-vo/ingredient-vo';
import { TypeIngredient } from './../../enumeration/type-ingredient.enum';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IngredientRepositoryService } from '../../repository/ingredient-repository.service';

@Component({
  selector: 'app-ingredients-ajout-modif',
  templateUrl: './ingredients-ajout-modif.component.html',
  styleUrls: ['./ingredients-ajout-modif.component.css'],
})
export class IngredientsAjoutModifComponent implements OnInit {
  // tslint:disable-next-line: no-output-on-prefix
  @Output()
  onClose: EventEmitter<boolean> = new EventEmitter();

  formAjoutModifIngredient: FormGroup;
  typeIngredient: any;
  ingredient = new IngredientVo();

  constructor(
    private fc: FormBuilder,
    private readonly ingredientRepository: IngredientRepositoryService
  ) {
    this.formAjoutModifIngredient = this.fc.group({
      ingredientId: [null],
      nomIngredient: [''],
      typeIngredient: [''],
    });
  }

  ngOnInit() {
    this.chargerListeTypeIngredient();
  }

  chargerListeTypeIngredient() {
    const StringIsNumber = (value) => isNaN(Number(value)) === false;

    this.typeIngredient = Object.keys(TypeIngredient)
      .filter(StringIsNumber)
      .map((key) => TypeIngredient[key]);
  }

  ajouterIngredient() {
    this.ingredient.nomIngredient = this.formAjoutModifIngredient.value.nomIngredient;
    this.ingredient.typeIngredient = this.formAjoutModifIngredient.value.typeIngredient;
    if (this.ingredient != undefined) {
      this.ingredientRepository
        .ajouterIngredient(this.ingredient)
        .subscribe(() => this.onClose.emit(true));
    }
  }
}
