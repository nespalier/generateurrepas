import { TypeIngredient } from './../enumeration/type-ingredient.enum';
export class IngredientVo {
  ingredientId: number;
  nomIngredient: string;
  typeIngredient: TypeIngredient;
}
