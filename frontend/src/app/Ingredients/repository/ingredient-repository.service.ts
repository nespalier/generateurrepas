import { IngredientVo } from './../classes-vo/ingredient-vo';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class IngredientRepositoryService {
  adress = 'http://localhost:9090/ingredient/';
  constructor(private http: HttpClient, private route: Router) {}

  ajouterIngredient(ingredient: IngredientVo) {
    return this.http.post(this.adress, ingredient);
  }
}
