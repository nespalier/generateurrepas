import { RepasVo } from './../classes-vo/repas-vo';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class RepasRepositoryService {
  adress = 'http://localhost:9090/repas/';
  constructor(private http: HttpClient, private route: Router) {}

  getById(id: number) {
    return this.http.get(this.adress + id);
    
  }

  getAll(): any {
    return this.http.get(this.adress);
  }
}
