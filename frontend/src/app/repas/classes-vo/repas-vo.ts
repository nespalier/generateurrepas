import { TypeRepasEnum } from './../enumeration/type-repas-enum.enum';
import { JourEnum } from './../enumeration/jour-enum.enum';
import { SemaineEnum } from './../enumeration/semaine-enum.enum';
export class RepasVo {
    libelle: string;
    typeRepas: TypeRepasEnum;
}
