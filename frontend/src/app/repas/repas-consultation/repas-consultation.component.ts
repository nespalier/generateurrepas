import { RepasVo } from './../classes-vo/repas-vo';
import { RepasRepositoryService } from './../repository/repas-repository.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-repas-consultation',
  templateUrl: './repas-consultation.component.html',
  styleUrls: ['./repas-consultation.component.scss'],
})
export class RepasConsultationComponent implements OnInit {
  listRepas: RepasVo[];
  constructor(private readonly RepasRepo: RepasRepositoryService) {}

  ngOnInit() {
    this.chargerListeRepas();
  }

  chargerListeRepas() {
    this.RepasRepo.getAll().subscribe(
      repas=> {
        this.listRepas = repas
        console.log(this.listRepas)
      }

    )
  }
}
