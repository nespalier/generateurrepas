import { AppComponent } from './app.component';
import { AccueilComponent } from './accueil/accueil.component';
import { RepasConsultationComponent } from './repas/repas-consultation/repas-consultation.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {path:"",redirectTo:"/accueil",pathMatch:"full"},
  {path:"accueil",component:AppComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
